autocmd	BufNewFile	*.h	call	Generate_h_42()

function! Generate_h_42()
	let l:name = expand('%:f')
	let l:cmd = '/usr/bin/basename ' . l:name
	let l:newname = system(l:cmd)
	let l:newname = toupper(l:newname)
	let l:newname = substitute(l:newname, "\\.", "_", "g")
	let l:newname = substitute(l:newname, "\\n", "", "g")
	exe ":normal A" . "#ifndef " . l:newname . "\n# define " . l:newname . "\n\n\n\n#endif"
	if exists("*Insert_header_42")
		exe ":16"
	else
		exe ":4"
	endif
endfunction
