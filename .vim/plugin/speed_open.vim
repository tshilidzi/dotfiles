function!	Speed_open(split)
	let l:extension = ""
	if expand('%:e') ==? "c"
		let l:extension = ".h"
	endif
	if expand('%:e') ==? "h"
		let l:extension = ".c"
	endif
	let l:current_extension = "\\." . expand('%:e')
	let	l:file_open = substitute(expand('%:f'), l:current_extension, l:extension, "g")
	let l:cmd = '/usr/bin/basename "' . l:file_open . '" 2> /dev/null | tr -d "\n"'
	let l:file_open = system(l:cmd)
	for dir in ["." , ".."]
		let l:cmd = "find " . dir . " -type f -name " . l:file_open . " 2> /dev/null | tr '\n' ' ' | awk '{print $1}' | tr -d '\n'"
		let l:file = system(l:cmd)
		if filereadable(l:file)
			exe ":" . a:split . " " . l:file
			return
		endif
	endfor
	echo "No such file " . l:file_open . "."
endfunction
